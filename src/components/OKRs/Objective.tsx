import React, { useContext } from "react";
import { GoalsContext } from "../../context/GoalsContext";

const Objective = () => {
  const [, , objective, setObjective] = useContext(GoalsContext);
  return (
    <div>
      <h1> Objectives : </h1>
      <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Status</th>
            <th>Key Result</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>{objective.name}</td>
            <td>{objective.status}</td>
            <td>{objective.keyResult}</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

export default Objective;
