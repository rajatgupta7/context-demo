import React, { useState, useContext } from "react";
import { GoalsContext } from "../../context/GoalsContext";

const AddGoal = () => {
  //* Consume the Context using useContext(). Using this one line of code, I've everything that was created in that context.
  const [goals, setGoals] = useContext(GoalsContext);
  const [newGoal, setNewGoal] = useState({
    name: "",
    progress: "0",
    status: "",
  });

  const addGoal = (e: any) => {
    e.preventDefault();
    //*Add the goal:
    setGoals((prevGoals: any) => [...prevGoals, newGoal]);
  };

  return (
    <form onSubmit={addGoal}>
      <input
        type="text"
        name={"name"}
        placeholder={"Name"}
        onChange={(e) => setNewGoal({ ...newGoal, name: e.target.value })}
      />
      <input
        type="text"
        name={"progress"}
        placeholder={"Progress"}
        onChange={(e) => setNewGoal({ ...newGoal, progress: e.target.value })}
      />
      <input
        type="text"
        name={"status"}
        placeholder={"Status"}
        onChange={(e) => setNewGoal({ ...newGoal, status: e.target.value })}
      />
      <button> +Add</button>
    </form>
  );
};

export default AddGoal;
