import React, { useContext } from "react";
import { GoalsContext } from "../../context/GoalsContext";
import AddGoal from "./AddGoal";

const Goals = () => {
  //* Consume the Context using useContext(). Using this one line of code, I've everything that was created in that context.
  const [goals, setGoals] = useContext(GoalsContext);

  return (
    <div>
      <h1>No of Goals I got : {goals.length} </h1>
      <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Progress</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {goals.map((goal: any, index: number) => {
            return (
              <tr key={index}>
                <td>{goal.name}</td>
                <td>{goal.progress}</td>
                <td> {goal.status} </td>
              </tr>
            );
          })}
        </tbody>
      </table>

      <h2>Add a Goal...</h2>
      <AddGoal />
    </div>
  );
};

export default Goals;
