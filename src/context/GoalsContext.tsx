import React, { createContext, useState } from "react";

//* have to define a prop type to the context that it'll use it as the default value:
type StrongType = {
  key: string;
  values: number;
};

//* For simplicity, I'm keeping the default StateValue loosely typed:
let defaultStateType: any = "Oh, this is the default state in the context";

// * Create the Goals Context:
export const GoalsContext = createContext(defaultStateType);

//* Create a Provider so that anything under this Component can access the state context.

export const GoalsProvider = (components: any) => {
  //* Creating some states :
  const [objective, setObjective] = useState({
    name: "Special Objectives",
    status: "On Track",
    keyResult: "KR 1",
  });
  const [goals, setGoals] = useState([
    {
      name: "Critical Goal 1",
      progress: "25",
      status: "On Track",
    },
    {
      name: "Critical Goal 2",
      progress: "52",
      status: "On Track",
    },
    {
      name: "Normal Goal 1",
      progress: "78",
      status: "On Track",
    },
    {
      name: "Normal Goal 2",
      progress: "90",
      status: "On Track",
    },
  ]);

  return (
    <GoalsContext.Provider value={[goals, setGoals, objective, setObjective]}>
      {components.children}
    </GoalsContext.Provider>
  );
};

export default GoalsContext;
