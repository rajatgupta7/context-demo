import React, { Fragment } from "react";
import "./App.css";
import Goals from "./components/Goals/Goals";
import { GoalsProvider } from "./context/GoalsContext";
import Objective from "./components/OKRs/Objective";

function App() {
  return (
    <Fragment>
      <div className="App">
        <h1> Context Demo </h1>
        <GoalsProvider>
          <Goals />
          <Objective />
        </GoalsProvider>
      </div>
    </Fragment>
  );
}

export default App;
